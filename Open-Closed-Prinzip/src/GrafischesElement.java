/**
 * Dieser Code stammt aus dem Buch:
 *
 * Joachim Goll
 * "Architektur- und Entwurfsmuster der Softwaretechnik"
 * 2. Auflage
 * Springer Verlag Wiesbaden
 * ISBN 978-3-658-05531-8
 *
 * und dient nur zur Veranschaulichung des von mir im Haupseminar vorgestelltem Open-Closed-Prinzips.
 *
 * Die Java-Dateien zum Buch können unter folgender Webseite heruntergeladen werden:
 * http://www.it-designers-gruppe.de/forschung-und-lehre/lehrbuecher/aem-buch-downloads/
 *
 */


// Datei: GrafischesElement.java
import java.awt.Graphics;

public abstract class GrafischesElement
{
   // Schwerpunktskoordinaten
   protected int x, y;

   // hier koennen weitere Eigenschaften, wie bspw. Fuellfarbe
   // definiert werden.

   // Konstruktor mit Schwerpunktskoordinaten
   public GrafischesElement(int x, int y)
   {
       this.x = x;
       this.y = y;
   }

   // Methode zur Verschiebung des Schwerpunktes; durch final als
   // leaf-Methode deklariert.
   final public void verschiebe(int x, int y) 
   {
      this.x += x;
      this.y += y;
   }

   final public int getX()
   {
      return x;
   }

   final public int getY() 
   {
      return y;
   }

   // Methodenkopf der Darstellungsroutine; Graphics ist eine 
   // Klasse, welche das Auftragen von Grafiken auf Komponenten
   // erlaubt. In der zeichne()-Methode wird anschliessend das
   // darzustellende Element auf das Graphics-Objekt aufgetragen.
   public abstract void zeichne(Graphics g);

   // Methode zum Ueberpruefen, ob Punkt im Element liegt.
   public abstract boolean liegtPunktImElement (int x, int y);
}
