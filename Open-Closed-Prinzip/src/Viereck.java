/**
 * Dieser Code stammt aus dem Buch:
 *
 * Joachim Goll
 * "Architektur- und Entwurfsmuster der Softwaretechnik"
 * 2. Auflage
 * Springer Verlag Wiesbaden
 * ISBN 978-3-658-05531-8
 *
 * und dient nur zur Veranschaulichung des von mir im Haupseminar vorgestelltem Open-Closed-Prinzips.
 *
 * Die Java-Dateien zum Buch können unter folgender Webseite heruntergeladen werden:
 * http://www.it-designers-gruppe.de/forschung-und-lehre/lehrbuecher/aem-buch-downloads/
 *
 */


// Datei: Viereck.java
import java.awt.Graphics;
import java.awt.Rectangle;

public class Viereck extends GrafischesElement 
{ 
   int laenge, hoehe;
   Rectangle viereck;
   
   public Viereck(int x, int y, int laenge, int hoehe) 
   {
      super(x, y);
      this.laenge = laenge;
      this.hoehe = hoehe;
   }
   
   // Implementierte zeichne()-Methode der Basisklasse
   public void zeichne(Graphics g) 
   {
      // Viereck erstellen
      viereck = new Rectangle(this.x - (int) (0.5 * laenge),
                      this.y - (int) (0.5 * hoehe), laenge, hoehe);

      // Viereck zeichnen
      g.fillRect(viereck.x, viereck.y, viereck.width, 
                 viereck.height);
   }

   public boolean liegtPunktImElement(int x, int y) 
   {
      return viereck.contains(x,y);
   }
}
