Hier finden Sie die von mir präsentierte Beispiele zu den Entwurfsmustern:
 - Observer
 - Open-Closed-Prinzip
 
 Die Codebeispiele stammen aus dem Buch:
 
  Joachim Goll <br>
 "Architektur- und Entwurfsmuster der Softwaretechnik" <br>
  2.Auflage <br>
 Springer Verlag Wiesbaden <br>
 ISBN 978-3-658-05531-8
 
 und dienen nur zur Verständlichkeit der Entwurfsmuster.