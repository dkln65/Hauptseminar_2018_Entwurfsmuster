/**
 * Dieser Code stammt aus dem Buch:
 *
 * Joachim Goll
 * "Architektur- und Entwurfsmuster der Softwaretechnik"
 * 2. Auflage
 * Springer Verlag Wiesbaden
 * ISBN 978-3-658-05531-8
 *
 * und dient nur zur Veranschaulichung des von mir im Haupseminar vorgestelltem Observer-Patterns.
 *
 * Die Java-Dateien zum Buch können unter folgender Webseite heruntergeladen werden:
 * http://www.it-designers-gruppe.de/forschung-und-lehre/lehrbuecher/aem-buch-downloads/
 *
 */


// Datei: Abonnent.java
public class Abonnent implements IBeobachter
{
   private String name;

   public Abonnent (String name)
   {
      this.name = name;
   }

   public void aktualisieren (IBeobachtbar b) 
   {
      System.out.println ("Neue Nachricht fuer " + name + "");
      System.out.println ("Nachricht: " + b.gibZustand());
   }
}
