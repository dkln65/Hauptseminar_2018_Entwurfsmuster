/**
 * Dieser Code stammt aus dem Buch:
 *
 * Joachim Goll
 * "Architektur- und Entwurfsmuster der Softwaretechnik"
 * 2. Auflage
 * Springer Verlag Wiesbaden
 * ISBN 978-3-658-05531-8
 *
 * und dient nur zur Veranschaulichung des von mir im Haupseminar vorgestelltem Observer-Patterns.
 *
 * Die Java-Dateien zum Buch können unter folgender Webseite heruntergeladen werden:
 * http://www.it-designers-gruppe.de/forschung-und-lehre/lehrbuecher/aem-buch-downloads/
 *
 */


// Datei: TestBeobachter.java
public class TestBeobachter 
{
   public static void main (String[] args) 
   {
      Newsletter newsletter = new Newsletter();
      Abonnent andreas = new Abonnent ("Andreas");
      Abonnent birgit = new Abonnent ("Birgit");

      newsletter.anmelden (andreas);
      newsletter.anmelden (birgit);
      newsletter.aendereNachricht ("Neuigkeit 1");
      System.out.println();

      newsletter.abmelden (andreas);
      newsletter.aendereNachricht ("Neuigkeit 2");

      newsletter.abmelden (birgit);
      newsletter.aendereNachricht ("Neuigkeit 3");
   }
}
