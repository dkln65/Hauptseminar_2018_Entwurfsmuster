/**
 * Dieser Code stammt aus dem Buch:
 *
 * Joachim Goll
 * "Architektur- und Entwurfsmuster der Softwaretechnik"
 * 2. Auflage
 * Springer Verlag Wiesbaden
 * ISBN 978-3-658-05531-8
 *
 * und dient nur zur Veranschaulichung des von mir im Haupseminar vorgestelltem Observer-Patterns.
 *
 * Die Java-Dateien zum Buch können unter folgender Webseite heruntergeladen werden:
 * http://www.it-designers-gruppe.de/forschung-und-lehre/lehrbuecher/aem-buch-downloads/
 *
 */


// Datei: Newsletter.java
import java.util.Vector;

public class Newsletter implements IBeobachtbar 
{
   private Vector<IBeobachter> abonnenten = new Vector<IBeobachter>();

   private String nachricht;

   public void aendereNachricht (String neueNachricht)
   {
      nachricht = neueNachricht;
      benachrichtigen();
   }

   public void abmelden (IBeobachter beobachter) 
   {
      abonnenten.remove (beobachter);
   }

   public void anmelden (IBeobachter beobachter) 
   {
      abonnenten.add (beobachter);
   }

   private void benachrichtigen() 
   {
      for (IBeobachter beobachter : abonnenten)
      {
         beobachter.aktualisieren (this);
      }
   }
   public String gibZustand() 
   {
      return nachricht;
   }
}
